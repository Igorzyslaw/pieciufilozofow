﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace PięciuFilozofów
{

    




   public class Filozof
    {
        public int priorytet = 0;
        int id;
        bool rekaL = false;
        bool rekaP = false;
        public int poziomNajedzenia = 0;
        long czasZaglodzenia = 100; //(ms)
        int wid1 = -1;
        int wid2 = -1;
        Random r = new Random();
        public int priorytetKarmienia = 0;
        public bool czyPrzezyl = true;
        

        public long ileNieJadl = 0;







        public void jedzenie()
        {
            this.poziomNajedzenia++;
        }
        public int getId()
        {
            return id;
        }
        void initWid()
        {
            wid1 = this.id;
            if (this.id == 0) wid2 = Form1.liczbaFilozofow - 1;
            else wid2 = this.id - 1;

           
        }

        public Filozof(int id_)
        {
            this.id = id_;
            initWid();
            priorytet = id_;
        }

        public  void start()
        {
            var stopwatch = new Stopwatch();
            int licznik = 0;
            int ilePowt = 1000;
            stopwatch.Start();
            while (licznik < ilePowt)
            {


                if (wezWidelce())
                {
                    jedzenie();
                    zwrocWidelce();
                    wyswStanNajedzenia();
                    priorytet = 0;
                    stopwatch.Restart();
                }
                else
                {
                    zwrocWidelce();
                    priorytet++;
                    // if((int)Form1.thr[this.id].Priority < 4) Form1.thr[this.id].Priority++ ;
                   // Console.WriteLine("Filozof nr: " + this.id + " nie zjadł :(");
                    
                }
                    ileNieJadl = stopwatch.ElapsedMilliseconds;
                if (ileNieJadl > czasZaglodzenia && licznik>0)
                {
                    Console.WriteLine("##### Filozof nr: " + this.id + " ZMARŁ!!!");
                    stopwatch.Stop();
                    czyPrzezyl = false;
                    Form1.thr[this.id].Abort();
                }
                licznik++;
                
            }
            stopwatch.Stop();
           

        }

        public bool wezWidelce()
        {
            bool czyOba = false;
            

            Filozof fil = Form1.filozof[this.id];
            Filozof fil1;
            Filozof fil2;

            int min = 0;
            int max = 0;

            if (priorytet == 0)
            {
                min = 20;
                max = 70;
            }
            else
            {
                min = 100 / priorytet;
                max = min + 20 / priorytet;
            }
           
           


            Thread.Sleep(r.Next(min,max));
            bool czyJesc = false;


            if (this.id != 0 && this.id != Form1.liczbaFilozofow - 1)
            {
                fil1 = Form1.filozof[this.id - 1];
                fil2 = Form1.filozof[this.id + 1];
            }
            else if (this.id == 0)
            {
                fil1 = Form1.filozof[Form1.liczbaFilozofow - 1];
                fil2 = Form1.filozof[this.id + 1];
            }
            else
            {
                fil1 = Form1.filozof[this.id - 1];
                fil2 = Form1.filozof[0];
            }


            if (priorytet >= fil1.priorytet )
            {
                czyJesc = true;
            }
            else if (priorytet >= fil2.priorytet )
            {
                
                czyJesc = true;
            }
           







            if (Form1.widelec[wid1].przywlaszcz(this.id) && Form1.widelec[wid2].przywlaszcz(this.id) && czyJesc == true) czyOba = true;
        
            return czyOba;
        }

        private void wr(String txt)
        {
            Console.WriteLine(txt);
        }
        private void zwrocWidelce()
        {
            Form1.widelec[wid1].zwolnij();
            Form1.widelec[wid2].zwolnij();

        }
        void wyswStanNajedzenia()
        {

            Console.WriteLine("Filozof nr: "+this.id+" "+poziomNajedzenia);
            
        }

    }

}
