﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace PięciuFilozofów
{
   public class Widelec
    {
        int id;
        public int idFilozofa = -1;
        public static readonly object m_lock = new object();

        public Widelec(int id)
        {
            this.id = id;
        }

        public bool wolny()
        {
            if (idFilozofa == -1) return true;
            return false;
        }
        public bool przywlaszcz(int idFil)
        {
                    
                if (this.idFilozofa == -1)
                {
                Monitor.TryEnter(m_lock);
                this.idFilozofa = idFil;
                    
                    Monitor.Exit(m_lock);
                return true;
                }
                else return false;
            
            
        }
        public void zwolnij()
        {
            this.idFilozofa = -1;
            
        }
        
    }
}
